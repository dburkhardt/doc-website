---
title: "About"
---

# About

Generated using [Hugo](https://gohugo.io) and [Doc *et al*](https://gitlab.com/vejnar/hugo-theme-docetal) theme.

## License

Linux Install Notes is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

![CC BY-SA](images/by-sa.svg)

Copyright © Charles E. Vejnar

## Trademark Legal Notice

All product names, logos, trademarks, registered trademarks, and brands are property of their respective owners. All company, product and service names used in this website are for identification purposes only. Use of these names, logos, trademarks, and brands does not imply endorsement.

The registered trademark Linux® is used pursuant to a sublicense from the Linux Foundation, the exclusive licensee of Linus Torvalds, owner of the mark on a world-wide basis.

All other trademarks cited herein are the property of their respective owners.
