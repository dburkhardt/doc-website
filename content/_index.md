---
title: ""
---

{{< rawhtml >}}

<div class="front-banner">
  <img class="img-max" src="images/logo.svg" width="700rem">
  <div class="title">Building servers with Linux</div>
  <div class="links">
    <a class="link" href="#features">Features</a>
    <a class="link" href="doc">Getting started</a>
    <a class="link" href="https://gitlab.com/vejnar/doc-website">Contribute</a>
  </div>
</div>

<div class="features">
  <a name="features"></a>
  <div class="features-title">Features</div>
  <div class="container">
    <div class="feature-box">
      <i class="fas fa-server"></i>
      <div class="feature-box-title">Servers</div>
      <div class="feature-box-row">How to install a computing, a file-sharing, a web, or a backup server?</div>
    </div>
    <div class="feature-box">
      <i class="fab fa-osi"></i>
      <div class="feature-box-title">Open source</div>
      <div class="feature-box-row">All solutions are based on open source software, such as <a href="https://www.archlinux.org">ArchLinux</a> or <a href="https://www.debian.org">Debian</a>. Our content is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons</a> license.</div>
    </div>
    <div class="feature-box">
      <i class="fas fa-clipboard-list"></i>
      <div class="feature-box-title">Step-by-step guides</div>
      <div class="feature-box-row">Notes are sharp step-by-step installation guides. Read the original project documentation for details.</div>
    </div>
    <div class="feature-box">
      <i class="fas fa-handshake"></i>
      <div class="feature-box-title">Cohesive</div>
      <div class="feature-box-row">All guides are compatible with each other. Used together, you will get a fully functional solution.</div>
    </div>
  </div>
</div>

{{< /rawhtml >}}
