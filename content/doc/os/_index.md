---
title: "OS"
weight: 2
LastModifInFooter: false
---

# Installing Operating Systems

* [ArchLinux](install_archlinux)
* [Debian](install_debian)
