---
title: "Install Archlinux"
description: "Notes on how to install Archlinux"
weight: 10
---

# Archlinux installation

{{< notice "warning" >}}
These are notes summarizing how to install Archlinux. They are **not** a complete installation howto, which is well covered in the official Archlinux [Installation guide](https://wiki.archlinux.org/index.php/Installation_guide).
{{< /notice >}}

{{< notice "tip" >}}
Configuration files are [available](files) for download.
{{< /notice >}}

## Installation

1. Prepare installation [media](https://www.archlinux.org/download) and boot on it.
2. Installation

    1. Partition with **parted**. First partition is for EFI. Last partition is for Swap. Both are not mandatory. Adapt the middle partition size to your drive (here ~200GB). The swap partition should be at least the size of your RAM.
        ```bash
        mklabel gpt
        mkpart primary 1MiB 500MiB
        mkpart primary 500MiB 200GB
        mkpart primary 200GB 100%
        set 1 boot on
        ```
        To align partitions with drive sectors, the partition *Start* should be 2048s or with B unit 1047576, or with MiB unit exactly 1.00.

    2. Format partitions
        ```bash
        mkfs.vfat -F 32 -n ESP /dev/sda1
        mkfs.ext4 -L archroot /dev/sda2
        tune2fs -m1 /dev/sda2             # 1% reserved space
        tune2fs -i 6m /dev/sda2           # Check interval (6 months)
        mkswap -L archswap /dev/sda3
        ```

    3. Mount partitions

        * Main partition (discard option only for SATA SSD; not for NVMe or HDD).
            ```bash
            mount -o discard /dev/sda2 /mnt
            ```
        * EFI system partition (ESP) (see below for *systemd-boot*).
        * Swap
            ```bash
            swapon /dev/sda3
            ```

    4. [Optional] Update [Pacman mirror list](https://wiki.archlinux.org/index.php/Mirrors) using [reflector](https://xyne.archlinux.ca/projects/reflector) with one of these options:

        * Fast option: Get top 10 mirrors scored by Archlinux
            ```bash
            reflector --save /etc/pacman.d/mirrorlist --country us --score 10
            ```
        * Accurate option: Get top 10 mirrors scored by download speed from your location
            ```bash
            reflector --save /etc/pacman.d/mirrorlist --country us --number 10 --sort rate
            ```

        List of countries can be obtained using `reflector --list-countries` to change the `--country` option.

    5. Connect to internet **if** it wasn't done during boot

    6. Install packages
        ```bash
        pacstrap /mnt base base-devel linux linux-firmware nano dosfstools nftables openssh rsync tree
        ```

## Preliminary configuration

### Within the new filesystem

1. Chroot. Change root into the new system:
    ```bash
    arch-chroot /mnt
    ```

2. Hostname
    ```bash
    echo "<hostname>" > /etc/hostname
    ```

3. Bootloader: We use *systemd-boot*

    systemd-boot is integrated into systemd. While simple and robust, it can only read from the [ESP](https://wiki.archlinux.org/index.php/EFI_system_partition), i.e. no ext4 driver is integrated. To have access to the kernel and initramfs, one can either (i) bind (with `mount --bind`) the `ESP/arch` folder to `/boot`. But with this solution `/boot` is a FAT partition with limited functionality. Or (ii) add a hook to Pacman to copy the kernel and initramfs to the ESP, as explained here.

    | File                                           | Description              |
    | ---------------------------------------------- | ------------------------ |
    | [loader.conf](files/loader.conf)               | Main systemd-boot config |
    | [arch.conf](files/arch.conf)                   | Archlinux loader         |
    | [check-esp.hook](files/check-esp.hook)         | Pacman ESP hook          |
    | [copy-boot-esp.hook](files/copy-boot-esp.hook) | Pacman ESP hook          |

    * Mount ESP on `/efi` and copy kernel and initramfs to it.
        ```bash
        mkdir /efi
        mount /dev/sda1 /efi
        mkdir /efi/arch
        cp /boot/*img /boot/vm* /efi/arch
        ```

    * Install systemd-boot
        ```bash
        bootctl --path /efi --no-variables install
        ```

    * Configure systemd-boot.
        * Copy [loader.conf](files/loader.conf) in `/efi/loader`,
        * Copy [arch.conf](files/arch.conf) in `/efi/loader/entries`.

    * Configure pacman hooks.
        * Create folder `/etc/pacman.d/hooks`,
        * Copy [check-esp.hook](files/check-esp.hook) and [copy-boot-esp.hook](files/copy-boot-esp.hook) in it.

    Optional:

    * Install [microcode](https://wiki.archlinux.org/index.php/Microcode) for AMD or Intel CPU loaded at boot.
        1. Install package `intel-ucode` for Intel or `amd-ucode` for AMD.
        2. In `/efi/loader/entries/arch.conf`, add the bold line (replace *intel* by *amd* if applicable):
            ```markdown
            linux          /arch/vmlinuz-linux
            **initrd         /arch/intel-ucode.img**
            initrd         /arch/initramfs-linux.img
            ```
        3. Copy microcode to EFI
            ```bash
            cp /boot/*img /efi/arch
            ```

        Loaded microcode can be check using `dmesg | grep microcode` or reading `/sys/devices/system/cpu/cpu0/microcode/version` after rebooting.

    * For Microsoft Windows, systemd-boot must be setup as main bootloader:

        * Issue as **admin** in terminal: `bcdedit /set {bootmgr} path \EFI\systemd\systemd-bootx64.efi`
        * Turn-off *Fast Startup*.

4. Configuring Locals

    * Uncomment local in `/etc/locale.gen` (en_US.UTF-8 and/or fr_FR.UTF-8)

    * Run
        ```bash
        echo "LANG=\"en_US.UTF-8\"" > /etc/locale.conf
        locale-gen
        ```

    * Time
        ```bash
        ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime
        ```

5. For NVMe SSD, enable periodic trim by enabling *fstrim.timer* systemd timer.

6. Set root password with `passwd`.

7. Exit arch-chroot.

### Final step & reboot

1. Creating fstab
    ```bash
    genfstab -Lp /mnt > /mnt/etc/fstab
    ```

2. Unmount install partitions:
    ```bash
    umount -R /mnt
    ```

3. Reboot

## Configuration

### Network

1. Nftables (firewall). Copy [nftables.conf](files/nftables.conf) in `/etc` and enable *nftables* systemd service.

2. SSH. Enable *sshd* systemd service.

3. Network interface

    * Networkd (systemd) - *One wired fixed*

        1. Create */etc/systemd/network/wired.network* (replace interface name *enp1s0* with yours. You can list interfaces using `ip link`):
            ```
            [Match]
            Name=enp1s0
            [Network]
            DHCP=yes
            ```

        2. Enable *systemd-networkd* and *systemd-resolved* (to get DNS from DHCP) services.

        3. Before starting network or rebooting, remove if it exists `/etc/resolv.conf`

    * Netctl - *Complex connections*

    * NetworkManager - *Laptop*

        1. Install *networkmanager* packages
        2. Enable *NetworkManager* systemd service
        3. If systemd is starting service(s) that require an active connection, enable service *NetworkManager-wait-online*

4. Time synchronization with timesyncd (systemd)
    ```bash
    timedatectl set-ntp true
    ```

### Graphical desktop environment

1. Install [KDE](https://www.kde.org) & enable SDDM (Simple Desktop Display Manager)
    ```bash
    pacman -Sy plasma-meta
    systemctl enable sddm
    ```
    If necessary, set SDDM keyboard config (for a french keyboard): `localectl set-x11-keymap fr`

2. Install either `kde-applications-meta` (full install) or `kdebase-meta` (minimal install).

3. With Intel CPU, for video hardware decoding, install *libva-intel-driver* or *intel-media-driver* for Broadwell+.

### User config

1. Add a normal user (adapt or remove user and group ID)
    ```bash
    groupadd -g 1000 charles
    useradd -m -u 1000 -g 1000 charles
    passwd charles
    ```

2. In KDE System Settings, the Breeze theme for GTK can be configured. But the cursor default theme is not Breeze. To fix this, in `~/.icons/default/index.theme`, add:
    ```
    [Icon Theme]
    Inherits=breeze_cursors
    ```
    Then issue `ln -s /usr/share/icons/breeze_cursors/cursors ~/.icons/default/cursors`.

3. Keyboard layout can be configured directly in KDE System Settings.

    * With an **canadian** Apple keyboard:
        * Keyboard model is "Generic 104-key PC"
        * The ~ and < are reversed. Copy [Xmodmap](files/Xmodmap) in user's home as `~/.Xmodmap`.
        * Install package `xorg-xmodmap` if necessary.

4. Fonts and anti-aliasing.

    * Copy [Xresources](files/Xresources) in user's home as `~/.Xresources`.
    * Copy [fonts.conf](files/fonts.conf) in `~/.config/fontconfig`.

5. Bluetooth. Enable *bluetooth* systemd service.

6. Sound

    * Display sound cards with `aplay -l` (from `alsa-utils` package).

    * If there are two cards, their order will be random as detection by Udev is random. To maintain the main soundcard at the first position, add in `/etc/modprobe.d/alsa.conf` (replace `snd-ice1712` with sound card driver):
        ```
        options snd-ice1712 index=0
        ```

    * PulseAudio defaults hardware to use 44.1 kHz or 48 kHz sample rate. In case of crack sounds and if the sound card can handle multiple sample rates (i.e. 44.1, 48 or 96Khz), set PulseAudio to not systematically resample sound. Add to `~/.config/pulse/daemon.conf`:
        ```
        avoid-resampling = yes
        ```

7. Printing

    * Install `cups` package
    * Enable *org.cups.cupsd.service* systemd service
    * Configure printer(s) at *http://localhost:631*

8. Recommended packages
    ```bash
    pacman -S ttf-croscore
    pacman -S chromium firefox
    pacman -S audacious mpv
    pacman -S libreoffice-fresh hunspell-en_US hunspell-fr hyphen-en hyphen-fr
    pacman -S jre10-openjdk icedtea-web
    ```
