---
title: "Install Debian"
description: "Notes on how to install Debian"
weight: 11
---

# Debian installation

This guide is intended to describe the installation of [Debian](https://www.debian.org) for a server (i.e without a desktop environment). Since the installation is straightforward using the Debian installer, this guide concentrates on post-installation configuration.

{{< notice "tip" >}}
Configuration files are [available](files) for download.
{{< /notice >}}

## Installation

1. Download the Debian [image](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd) (`debian-...-netinst.iso`) and prepare a bootable media.
2. Boot on install media.
3. Install Debian step-by-step using Debian installer. For a server, do a minimal installation. A full [guide](https://www.debian.org/releases/stable/installmanual) is available for more details.

## Configuration

### Network

1. Nftables (firewall).
    1. Install Nftables
        ```bash
        apt-get install nftables
        ```

    2. Copy [nftables.conf](../install_archlinux/files/nftables.conf) in `/etc`

    3. Enable *nftables* systemd service.

2. SSH. Enable *sshd* systemd service.

3. Networkd (systemd)

    1. Disable */etc/network*, default system managing network interfaces in Debian
        ```bash
        mv /etc/network/interfaces /etc/network/interfaces.save
        ```

    2. Create */etc/systemd/network/wired.network* (replace interface name *enp1s0* with yours. You can list interfaces using `ip link`):
        ```ini
        [Match]
        Name=enp1s0
        [Network]
        DHCP=yes
        ```

    3. Enable *systemd-networkd* and *systemd-resolved* (to get DNS from DHCP) services.

    4. Before starting network or rebooting, remove if it exists `/etc/resolv.conf`

4. Time synchronization with timesyncd (systemd)
    ```bash
    timedatectl set-ntp true
    ```

### Microcode

Summary of [detailed](https://wiki.debian.org/Microcode) installation.

1. Enable *contrib* and *non-free* sources in /etc/apt/sources.list if they aren't already enabled. Starting with (example for *buster*):
    ```
    deb http://deb.debian.org/debian buster main
    deb-src http://deb.debian.org/debian buster main

    deb http://deb.debian.org/debian-security/ buster/updates main
    deb-src http://deb.debian.org/debian-security/ buster/updates main

    deb http://deb.debian.org/debian buster-updates main
    deb-src http://deb.debian.org/debian buster-updates main
    ```
    modify to:
    ```
    deb http://deb.debian.org/debian buster main contrib non-free
    deb-src http://deb.debian.org/debian buster main contrib non-free

    deb http://deb.debian.org/debian-security/ buster/updates main contrib non-free
    deb-src http://deb.debian.org/debian-security/ buster/updates main contrib non-free

    deb http://deb.debian.org/debian buster-updates main contrib non-free
    deb-src http://deb.debian.org/debian buster-updates main contrib non-free
    ```
2. Install the microcode package.
    * AMD
        ```bash
        apt-get update
        apt-get install amd64-microcode
        ```
    * Intel
        ```bash
        apt-get update
        apt-get install intel-microcode
        ```

### Automatic upgrades

Debian can automatically install software upgrades, including security updates. [Unattended Upgrades](https://wiki.debian.org/UnattendedUpgrades) maintain a system up-to-date without human intervention. The system also reboots automatically when necessary. Unattended Upgrades are installed by default (`unattended-upgrades` and `apt-listchanges` packages) and configured in `/etc/apt/apt.conf.d/50unattended-upgrades`.

1. To activate automatic upgrades, create the [20auto-upgrades](files/20auto-upgrades) file in `/etc/apt/apt.conf.d` directory.

2. To activate automatic reboot, uncomment and change to `true` the `Automatic-Reboot` in `/etc/apt/apt.conf.d/50unattended-upgrades` (eventually also change the reboot time):
    ```
    Unattended-Upgrade::Automatic-Reboot "true";
    ```

Logs of upgrades performed are recorded in `/var/log/unattended-upgrades`.
