---
title: "LVM"
description: "Logical Volume Management"
weight: 10
---

# LVM

## Introduction

LVM is a [Logical Volume Manager](https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)). It helps creating storage spaces not restricted by physical storage drives. This flexibility is achieved by a mapping between logical storage units and physical storage units.

File systems are setup on Logical Volumes (LV). Space from a Volume Group (VG) is attributed to a Logical Volume (LV), while Logical Volumes (LV) combine Physical Volumes (PV) together.

LVM can be setup either:
1. **Directly** on bare storage drives or,
2. on top of a **RAID** array.

{{< notice "tip" >}}
This guide explain how to setup LVM on non-root partitions. For more information, please refer to Archlinux [LVM](https://wiki.archlinux.org/index.php/LVM) page.
{{< /notice >}}

## Prepare storage

### Using bare drives

Using bare drives provides *no data redundancy*. The main advantage of this solution is that hard-drives of *different* sizes can be combined. Using this setup, hard-drives of increased capacity can be added as needed to an existing LVM to increase its storage size.

In this example, two hard-drives identified as `/dev/sda` and `/dev/sdb` are used:

![LVM](images/lvm.svg)

Create a partition using 100% of space on each hard-drive.
```bash
parted /dev/sda mklabel gpt mkpart primary 1MiB 100% set 1 lvm on print
parted /dev/sdb mklabel gpt mkpart primary 1MiB 100% set 1 lvm on print
```

### Using RAID

In contrast to bare drives, [RAID](https://en.wikipedia.org/wiki/RAID) provides data redundancy. To setup a RAID array, please follow these [instructions](../raid#setup-new-raid).

In this example, two hard-drives identified as `/dev/sda` and `/dev/sdb` are represented (please note that some RAID levels, like RAID5, require at least 3 drives):

![LVM](images/lvm_on_raid.svg)

## Create new LVM

1. Create the Volume Group *VolGroup1* using the two partitions just created. The Physical Volumes are created automatically for each partition using 100% of the space.
    * Using bare devices (see [above](#using-bare-drives)):
        ```bash
        vgcreate VolGroup1 /dev/sda1 /dev/sdb1
        ```
    * Using RAID array (see [above](#using-raid)):
        ```bash
        vgcreate VolGroup1 /dev/md0
        ```
2. Create Logical Volume (LV) *VolData* using space in *VolGroup1*.
    ```bash
    lvcreate -l 100%FREE VolGroup1 -n VolData
    ```
    This will use 100% of the available space in *VolGroup1*. To use a limited amount of space, replace `-l 100%FREE` by `-L 500G` or `-L 10T`. For example:
    ```bash
    lvcreate -L 500G VolGroup1 -n VolData
    ```
    {{< notice "tip" >}}
Multiple Logical Volumes (LV) can be created until *VolGroup1* is full.
{{< /notice >}}
3. Format the volume *VolData* (here using *XFS*).
    * Using bare devices (see [above](#using-bare-drives)):
        ```bash
        mkfs.xfs -L data /dev/VolGroup1/VolData
        ```
    * Using RAID array (see [above](#using-raid)).

        Formating on top of a RAID array requires proper alignment of the filesystem with the RAID. See commands in the [RAID](../raid#format-new-raid) help.

## Modify LVM

### Add physical space

#### Using bare drives

{{< notice "tip" >}}
This section applies only to LVM on bare drives not RAID.
{{< /notice >}}

In case a new hard-drive (or partition) needs to be added to an existing Volume Group (VG), VG can be extended.
1. Partition the new hard-drive, here `/dev/sdc`, to create `/dev/sdc1` using *parted* as seen [above](#using-bare-drives).
2. Add the new space to *VolGroup1* (the Physical Volume is created automatically).
    ```bash
    vgextend VolGroup1 /dev/sdc1
    ```
3. Eventually, extend the Logical Volume (LV) *VolData* to use this added space (see below).

### Extend volume size

If space is available in a Volume Group (VG), Logical Volume (LV) can be extended and associated filesystem as well. To add space to the Logical Volume *VolData*:
```bash
lvresize -l +100%FREE --resizefs VolGroup1/VolData
```
To use a limited amount of space, replace `-l 100%FREE` by `-L +500G` or `-L +10T`. For example:
```bash
lvresize -L +500G --resizefs VolGroup1/VolData
```
The option `--resizefs` will automatically resize the filesystem on *VolData*.
