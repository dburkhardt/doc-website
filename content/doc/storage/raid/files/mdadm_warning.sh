#!/bin/bash

event=$1
md_device=$2
device=$3

case $event in
    DegradedArray)
        level="emerg"
        msg="$md_device is running in DEGRADED MODE"
        ;;
    DeviceDisappeared)
        level="crit"
        msg="$md_device has DISAPPEARED"
        ;;
    Fail)
        level="emerg"
        msg="$md_device had an ACTIVE component FAIL ($device)"
        ;;
    FailSpare)
        level="crit"
        msg="$md_device had a SPARE component FAIL during rebuild ($device)"
        ;;
    MoveSpare)
        level="notice"
        msg="SPARE device $device has been MOVED to a new array ($md_device)"
        ;;
    NewArray)
        level="notice"
        msg="$md_device has APPEARED"
        ;;
    Rebuild??)
        level="warning"
        msg="$md_device REBUILD is now `echo $event|sed 's/Rebuild//'`% complete"
        ;;
    RebuildFinished)
        level="warning"
        msg="REBUILD of $md_device is COMPLETE or ABORTED"
        ;;
    RebuildStarted)
        level="warning"
        msg="RECONSTRUCTION of $md_device has STARTED"
        ;;
    SpareActive)
        level="crit"
        msg="$device has become an ACTIVE COMPONENT of $md_device"
        ;;
    SparesMissing)
        level="crit"
        msg="$md_device is MISSING one or more SPARE devices"
        ;;
    TestMessage)
        level="emerg"
        msg="TEST MESSAGE generated for $md_device"
        ;;
esac

logger -p $level "mdadm: $msg"
