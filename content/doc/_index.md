---
title: "Home"
weight: 1
LastModifInFooter: false
---

# Building servers with Linux

* Installing Operating Systems
    * [ArchLinux](os/install_archlinux)
    * [Debian](os/install_debian)
* Managing Storage
    * [LVM](storage/lvm)
    * [RAID](storage/raid)
